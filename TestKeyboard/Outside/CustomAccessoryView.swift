import Foundation
import UIKit
import RxSwift
import RxCocoa

class CustomAccessoryView: UIView {
    let disposeBag = DisposeBag()

    var observerAdded = false
    var onFrameChangeObserve: ((CGFloat) -> ())?
    var onKeyboardFrameChange: ((Notification) -> ())?

    var convertToView: UIView?
    var disableNotificationObserver = false

    var keyboardFrameDisposable: Disposable?

    var lastKnownYPosition: CGFloat = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isUserInteractionEnabled = false
    }

    override var frame: CGRect {
        didSet {
            for constraint in constraints {
                if constraint.firstAttribute == .height {
                    constraint.constant = frame.size.height
                    break
                }
            }
            UIView.performWithoutAnimation {
                self.layoutIfNeeded()
            }
        }
    }

    override func removeFromSuperview() {
        super.removeFromSuperview()
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        if let oldView = superview , observerAdded {
            oldView.removeObserver(self, forKeyPath: "center", context: nil)
        }
        if let newView = newSuperview {
            newView.addObserver(self, forKeyPath: "center", context: nil)
            self.observerAdded = true
        }

        super.willMove(toSuperview: newSuperview)
    }

    func parentViewDidAppear() {
        keyboardFrameDisposable = NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillChangeFrame).subscribeDisposableInController(disposeBag, onNext: { [unowned self] notification in
            self.onKeyboardFrameChange(notification)
        })
    }

    func parentViewWillDisappear() {
        if let disposable = keyboardFrameDisposable {
            disposable.dispose()
        }
    }

    func onKeyboardFrameChange(_ notification: Notification) {
        if !disableNotificationObserver {
            frame.size.height = 0
            onKeyboardFrameChange?(notification)
        }
    }

    func calculateLastKnownYPosition(keyboardFrame: CGRect, additionalHeight: CGFloat = 0) {
        lastKnownYPosition = convertToView?.convert(keyboardFrame, from: superview?.superview).origin.y ?? superview?.frame.origin.y ?? 0
        lastKnownYPosition += additionalHeight
        if let convertToView = convertToView {
            lastKnownYPosition = lastKnownYPosition > convertToView.bounds.height ? convertToView.bounds.height : lastKnownYPosition
        }
    }

//Observation

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let superview = object as? UIView, superview == self.superview, keyPath == "center", disableNotificationObserver {
            calculateLastKnownYPosition(keyboardFrame: superview.frame, additionalHeight: frame.size.height)
            onFrameChangeObserve?(lastKnownYPosition)
        }
    }

    deinit {
        if let superview = superview, observerAdded {
            superview.removeObserver(self, forKeyPath: "center", context: nil)
        }
    }

}
