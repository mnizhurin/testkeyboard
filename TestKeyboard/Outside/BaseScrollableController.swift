import UIKit
import RxSwift

class BaseScrollableController: UIViewController, UIScrollViewDelegate {
    let disposeBag = DisposeBag()

    @IBOutlet var bottomConstraint: NSLayoutConstraint?
    @IBOutlet var scrollView: UIScrollView?

    var disableNotificationDisposable: Disposable?
    var forceUpdateInsetsTouchesCounter = 0

    var enableBottomOffset = false

    var constantAdditionalHeight: CGFloat = 0
    var keyboardHeight: CGFloat = 0
    var previousObservedPosition: CGFloat = 0

    let accessoryView = CustomAccessoryView(frame: CGRect(x: 0, y: 0, width: 320, height: 0))

    override func viewDidLoad() {
        super.viewDidLoad()

        accessoryView.convertToView = view
        accessoryView.onFrameChangeObserve = { [unowned self] y in
            self.keyboardHeight = self.view.bounds.height - y

            self.bottomConstraint?.constant = self.keyboardHeight

            if !(self.scrollView?.isDragging ?? false), self.accessoryView.disableNotificationObserver {
                let dy = y - self.previousObservedPosition
                self.updateUI(duration: 0, options: .curveLinear, dy: dy)
            } else {
                self.view.layoutIfNeeded()
            }
            self.previousObservedPosition = y
        }
        accessoryView.onKeyboardFrameChange = { [unowned self] notification in
            let screenKeyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let keyboardFrame = self.view.convert(screenKeyboardFrame, from: nil)
            self.keyboardHeight = self.view.bounds.size.height - keyboardFrame.origin.y - (self.accessoryView.superview == nil ? 0 : self.accessoryView.bounds.height)

            let duration = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            let curve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
            self.updateUI(duration: duration, options: UIViewAnimationOptions(rawValue: curve << 16))
        }
    }

    func updateUI(duration: TimeInterval = 0.035, options: UIViewAnimationOptions = .curveLinear, viewToTakeHeightFrom: UIView? = nil, dy: CGFloat? = nil, scrollToBottom: Bool = false, completion: (() -> ())? = nil) {
        UIView.animate(withDuration: duration, delay: 0, options: options, animations: { [weak self] in
            self?.updateXUI(viewToTakeHeightFrom: viewToTakeHeightFrom, dy: dy, scrollToBottom: scrollToBottom)
        }, completion: { _ in  completion?()})
    }

    private func updateXUI(viewToTakeHeightFrom: UIView? = nil, dy: CGFloat? = nil, scrollToBottom: Bool = false) {
        if let scrollView = scrollView {
            let previousBottomInset = scrollView.contentInset.bottom

            self.view.layoutIfNeeded()
            constantAdditionalHeight = viewToTakeHeightFrom != nil ? viewToTakeHeightFrom!.bounds.height : constantAdditionalHeight
            let bottomInset = self.keyboardHeight + constantAdditionalHeight

            self.bottomConstraint?.constant = self.keyboardHeight

            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)
            scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)

            var dy = dy == nil ? previousBottomInset - bottomInset : dy!
            let visibleSize = scrollView.contentSize.height - scrollView.contentOffset.y
            var maxDy = visibleSize - (self.view.bounds.height - scrollView.contentInset.bottom)
            maxDy = max(0, maxDy)
            if dy < 0 {
                dy = max(dy, -maxDy)
            } else if dy > 0 {
                dy = min(dy, maxDy)
            }
            if scrollToBottom {
                let offsetToBottom = max(scrollView.contentSize.height - scrollView.bounds.size.height + scrollView.contentInset.top + bottomInset, 0)
                scrollView.contentOffset = CGPoint(x: 0, y: offsetToBottom)
            } else if enableBottomOffset {
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y - dy)
            }

            self.view.layoutIfNeeded()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        accessoryView.parentViewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        accessoryView.parentViewWillDisappear()
    }

    override var inputAccessoryView: UIView? {
        return accessoryView
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        accessoryView.frame.size.height = constantAdditionalHeight
        disableNotificationDisposable?.dispose()
        accessoryView.disableNotificationObserver = true

        forceUpdateInsetsTouchesCounter += 1
        if forceUpdateInsetsTouchesCounter == 2 {
            forceUpdateInsets()
            forceUpdateInsetsTouchesCounter = 0
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        accessoryView.frame.size.height = 0
        //.3 for observer to do it's work
        disableNotificationDisposable = Observable.just().delaySubscription(0.3, scheduler: MainScheduler.instance).subscribeDisposableInController(disposeBag, onNext: { [unowned self] in
            self.accessoryView.disableNotificationObserver = false

            self.forceUpdateInsetsTouchesCounter = 0
            self.forceUpdateInsets()
        })
    }

    func forceUpdateInsets() {
        let newBottomInset = keyboardHeight + constantAdditionalHeight
        scrollView?.contentInset = UIEdgeInsetsMake(0, 0, newBottomInset, 0)
        scrollView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, newBottomInset, 0)
    }

}

