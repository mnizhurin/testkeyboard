import Foundation
import UIKit
import RxSwift
import RxCocoa

class BaseKeyboardTrackerController: UIViewController, PseudoAccessoryViewDelegate {
    var keyboardDisposeBag = DisposeBag()

    @IBOutlet var bottomConstraint: NSLayoutConstraint?
    @IBOutlet var accessoryView: UIView?
    @IBOutlet var scrollView: UIScrollView?

    var beginFrame = CGRect.zero
    var endFrame = CGRect.zero
    var currentFrame = CGRect.zero

    var keyboardFrame: CGRect {
        if currentFrame.equalTo(CGRect.zero) {
            return CGRect.zero
        }

        return view.convert(currentFrame, from: nil)
    }

    var accessoryViewHeight: CGFloat {
        return accessoryView?.frame.size.height ?? 0
    }

    override func loadView() {
        super.loadView()

        view.becomeFirstResponder()
    }

    let pseudoAccessoryView = PseudoAccessoryView()

    override var inputAccessoryView: UIView? {
        return pseudoAccessoryView
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

//        pseudoAccessoryView.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.1)
        pseudoAccessoryView.backgroundColor = UIColor.clear
        pseudoAccessoryView.isUserInteractionEnabled = false

        pseudoAccessoryView.delegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        pseudoAccessoryView.height = accessoryViewHeight

//        NotificationCenter.default.rx.notification(.UIKeyboardWillChangeFrame).subscribe(onNext: { [weak self] notification in
//            self?.captureInfo(notification: notification)
//        }).addDisposableTo(keyboardDisposeBag)
//
//        NotificationCenter.default.rx.notification(.UIKeyboardDidChangeFrame).subscribe(onNext: { [weak self] notification in
//            self?.captureInfo(notification: notification)
//        }).addDisposableTo(keyboardDisposeBag)

    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    func updateUI() {
        UIView.animate(withDuration: 0.3) {
            let previousHeight = self.accessoryViewHeight

            self.view.layoutIfNeeded()

            self.pseudoAccessoryView.height = self.accessoryViewHeight

            if let scrollView = self.scrollView {
                let dy = previousHeight - self.accessoryViewHeight
                scrollView.contentOffset = CGPoint(x: 0, y: max(0, scrollView.contentOffset.y - dy))
            }
        }
    }

//    func captureInfo(notification: Notification) {
//        if let info = notification.userInfo, let beginFrame = info[UIKeyboardFrameBeginUserInfoKey] as? CGRect, let endFrame = info[UIKeyboardFrameEndUserInfoKey] as? CGRect {
//            self.beginFrame = beginFrame
//            self.endFrame = endFrame
//            currentFrame = endFrame
//        }
//    }

    func layoutSomething() {
        let keyboardFrame = self.keyboardFrame

        let inputHeight = pseudoAccessoryView.height
        var bottomPadding = -inputHeight

        if !keyboardFrame.equalTo(CGRect.zero) {
            bottomPadding += view.frame.size.height - keyboardFrame.origin.y
        }

//        print("INPUT: \(inputHeight), Y: \(keyboardFrame.origin.y)")

        bottomPadding = max(0, bottomPadding)

        if let bottomConstraint = bottomConstraint {
            let oldPadding = bottomConstraint.constant
            let dy = oldPadding - bottomPadding

            bottomConstraint.constant = bottomPadding

            if let scrollView = scrollView {
//                print("DY: \(oldPadding) - \(bottomPadding) = \(dy) --- \(scrollView.isDragging) --- \(scrollView.isDecelerating)")
                if !scrollView.isDragging, scrollView.contentOffset.y >= 0 {
                    scrollView.contentOffset = CGPoint(x: 0, y: max(0, scrollView.contentOffset.y - dy))
                }

                let inset = UIEdgeInsets(top: 0, left: 0, bottom: bottomPadding + inputHeight, right: 0)
                scrollView.contentInset = inset
                scrollView.scrollIndicatorInsets = inset
            }
        }

        view.layoutIfNeeded()
    }

    // PseudoAccessoryViewDelegate

    func pseudoAccessoryView(_ pseudoAccessoryView: PseudoAccessoryView, keyboardFrameDidChange frame: CGRect) {
//        print("OBSERVE: \(frame.origin.y)")
        currentFrame = frame
        layoutSomething()
    }

}
