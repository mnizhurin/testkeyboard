import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol KeyboardTrackerDelegate: class {
    func keyboardTracker(didUpdate tracker: KeyboardTracker)
}

class KeyboardTracker: PseudoAccessoryViewDelegate {
    static let instance = KeyboardTracker()

    var disposeBag = DisposeBag()

    var delegates = [KeyboardTrackerDelegate]()
    var isTracking = false

    var beginFrame = CGRect.zero
    var endFrame = CGRect.zero
    var currentFrame = CGRect.zero

    var keyboardView: UIView? {
        let windows = UIApplication.shared.windows

        if windows.count > 1 {
            for subview in windows[1].subviews {
                if subview.description.contains("<UIPeripheralHost") {
                    return subview
                } else if subview.description.contains("<UIInputSetContainerView") {
                    for subSubview in subview.subviews {
                        if subSubview.description.contains("<UIInputSetHost") {
                            return subSubview
                        }
                    }
                    break
                }
            }
        }

        return nil
    }

    private init() {
        getInitialKeyboardInfo()
    }

    deinit {
        stop()
    }

    func getInitialKeyboardInfo() {
        if let keyboardView = keyboardView {
            beginFrame = keyboardView.convert(keyboardView.bounds, to: nil)
            endFrame = beginFrame
        } else {
            beginFrame = CGRect.zero
            endFrame = CGRect.zero
        }

        for delegate in delegates {
            delegate.keyboardTracker(didUpdate: self)
        }
    }

    func captureInfo(notification: Notification) {
        if let info = notification.userInfo, let beginFrame = info[UIKeyboardFrameBeginUserInfoKey] as? CGRect, let endFrame = info[UIKeyboardFrameEndUserInfoKey] as? CGRect {
            self.beginFrame = beginFrame
            self.endFrame = endFrame
            currentFrame = endFrame
        }
    }

    func start() {
        if isTracking {
            return
        }

        isTracking = true

        getInitialKeyboardInfo()

        NotificationCenter.default.rx.notification(.UIKeyboardWillChangeFrame).subscribe(onNext: { [weak self] notification in
            self?.captureInfo(notification: notification)
        }).addDisposableTo(disposeBag)

        NotificationCenter.default.rx.notification(.UIKeyboardDidChangeFrame).subscribe(onNext: { [weak self] notification in
            self?.captureInfo(notification: notification)
        }).addDisposableTo(disposeBag)
    }

    func stop() {
        if !isTracking {
            return
        }

        isTracking = false

        disposeBag = DisposeBag()
    }

//    func createPseudoAccessoryView() -> PseudoAccessoryView {
//        return PseudoAccessoryView(delegate: self)
//    }

    func addDelegate(delegate: KeyboardTrackerDelegate) {
        delegates.append(delegate)
    }

    func removeDelegate(delegate: KeyboardTrackerDelegate) {
        for (index, existingDelegate) in delegates.enumerated() {
            if delegate === existingDelegate {
                delegates.remove(at: index)
                break
            }
        }
    }

    func keyboardFrame(forView view: UIView) -> CGRect {
        if currentFrame.equalTo(CGRect.zero) {
            return CGRect.zero
        }

        return view.convert(currentFrame, from: nil)
    }

    // PseudoAccessoryViewDelegate

    func pseudoAccessoryView(_ pseudoAccessoryView: PseudoAccessoryView, keyboardFrameDidChange frame: CGRect) {
        if !isTracking {
            return
        }

        currentFrame = frame

        for delegate in delegates {
            delegate.keyboardTracker(didUpdate: self)
        }
    }

}
