import Foundation
import ObjectMapper
import FMDB

class ContentMessage: Message {
    enum Status: Int {
        case uploading, pending, sent, failed
    }

    var status: Status = .sent

    var user: User?
    var membership: Membership?
    var reply: Reply?
    var forwardedMessage: ForwardedMessage?
    var wildcard: Wildcard?

    var text: String?
    var medias: [Media]? {
        didSet {
            if let medias = medias {
                mediaCount = medias.count
                for (index, media) in medias.enumerated() {
                    media.messageUuid = uuid
                    media.order = index + 1
                }
            }
        }
    }
    var mediaCount = 0

    var blocked = false
    var _saved: Bool?
    var saved: Bool { get { return _saved ?? false } set { _saved = newValue } }
    var likeCount = 0
    var _liked: Bool?
    var liked: Bool { return _liked ?? false }
    var spamCount = 0
    var _spammed: Bool?
    var spammed: Bool { return _spammed ?? false }

    var lastSentAt: Date?

    var editedAt: Date?

    var expanded = false

    var isUploading: Bool { return status == .uploading }
    var isPending: Bool { return status == .pending }
    var isSent: Bool { return status == .sent }
    var isFailed: Bool { return status == .failed }

    override var mine: Bool {
        if let user = user, let currentUser = UserManager.instance.currentUser {
            return user.id == currentUser.id
        } else if let membership = membership, let membershipId = AppSession.instance.getMembershipIdByChannelId(channelId: channelId) {
            return membership.id == membershipId
        } else {
            return false
        }
    }

    override var previewText: String {
        if deleted {
            return "Deleted message"
        } else if blocked || spammed {
            return "Marked as spam"
        } else if let text = text {
            return text
        } else if let wildcardType = wildcard?.type, wildcardType == .image {
            return "Image"
        } else if let medias = medias, let media = medias.first {
            return media.isImage ? "Image" : "Video"
        }
        return super.previewText
    }

    override var previewUserName: String? {
        if let user = user {
            return user.firstName
        }
        return super.previewUserName
    }

    override var previewMediaType: Media.MediaType? {
        if let medias = medias, let media = medias.first {
            return media.type
        }
        return super.previewMediaType
    }

    override init() {
        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        user                <- map["user"]
        membership          <- map["membership"]
        reply               <- (map["replied_message_data"], TransformOf<Reply, Any>(fromJSON: { Mapper<ContentMessage>().map(JSONObject: $0).map { Reply(message: $0) } }, toJSON: { _ in nil }))
        forwardedMessage    <- (map["forwarded_message_data"], TransformOf<ForwardedMessage, Any>(fromJSON: { Mapper<ForwardedMessage>().map(JSONObject: $0) }, toJSON: { _ in nil }))
        wildcard            <- (map["data.wildcard"], TransformOf<Wildcard, Any>(fromJSON: { Wildcard.map(data: $0) }, toJSON: { _ in nil }))

        text                <- map["data.text"]
        medias              <- map["data.media"]

        blocked             <- map["is_blocked"]

        _saved               <- map["bookmarked"]

        likeCount           <- map["likes_count"]
        _liked               <- map["liked"]

        spamCount           <- map["spam_reports_count"]
        _spammed             <- map["spam_reported"]

        editedAt <- (map["edited_at"], dateTransform)
    }

    override init(resultSet rs: FMResultSet) {
        super.init(resultSet: rs)

        text = rs.string(forColumn: DBMessage.text)

        if let messageStatus = Status(rawValue: Int(rs.int(forColumn: DBMessage.status))) {
            status = messageStatus
        }

        if let userId = intForColumn(rs, DBMessage.userId) {
            let user = User()
            user.id = userId
            user.firstName = rs.string(forColumn: DBMessage.userFirstName)
            user.lastName = rs.string(forColumn: DBMessage.userLastName)
            user.avatarThumb = rs.string(forColumn: DBMessage.userAvatarThumb)
            user.deleted = rs.bool(forColumn: DBMessage.userDeleted)
            self.user = user
        }

        if let membershipId = intForColumn(rs, DBMessage.membershipId) {
            let membership = Membership()
            membership.id = membershipId
            membership.username = stringForColumn(rs, DBMessage.membershipUsername)
            self.membership = membership
        }

        if let replyData = rs.data(forColumn: DBMessage.reply) {
            reply = Mapper<Reply>().map(JSONObject: NSKeyedUnarchiver.unarchiveObject(with: replyData))
        }

        if let forwardedMessageData = rs.data(forColumn: DBMessage.forwardedMessage) {
            forwardedMessage = Mapper<ForwardedMessage>().map(JSONObject: NSKeyedUnarchiver.unarchiveObject(with: forwardedMessageData))
        }

        if let wildcardData = rs.data(forColumn: DBMessage.wildcard) {
            wildcard = Wildcard.map(data: NSKeyedUnarchiver.unarchiveObject(with: wildcardData))
        }

        blocked = rs.bool(forColumn: DBMessage.blocked)
        _saved = rs.bool(forColumn: DBMessage.saved)
        likeCount = Int(rs.int(forColumn: DBMessage.likeCount))
        _liked = rs.bool(forColumn: DBMessage.liked)
        spamCount = Int(rs.int(forColumn: DBMessage.spamCount))
        _spammed = rs.bool(forColumn: DBMessage.spammed)

        mediaCount = Int(rs.int(forColumn: DBMessage.mediaCount))

        deleted = rs.bool(forColumn: DBMessage.deleted)
        lastSentAt = rs.date(forColumn: DBMessage.lastSentAt)
        editedAt = rs.date(forColumn: DBMessage.editedAt)
    }

    override func bindInsert() {
        super.bindInsert()

        args[DBMessage.mediaCount] = mediaCount

        if let user = user {
            args[DBMessage.userId] = user.id
            args[DBMessage.userFirstName] = user.firstName
            args[DBMessage.userLastName] = user.lastName ?? NSNull()
            args[DBMessage.userAvatarThumb] = user.avatarThumb ?? NSNull()
            args[DBMessage.userDeleted] = user.deleted
        }

        if let membership = membership {
            args[DBMessage.membershipId] = membership.id
            args[DBMessage.membershipUsername] = membership.username
        }

        args[DBMessage.reply] = reply.map { reply in
            return NSKeyedArchiver.archivedData(withRootObject: reply.toJSON())
        } ?? NSNull()
    }

    override func bindUpdate() {
        super.bindUpdate()

        args[DBMessage.text] = text ?? NSNull()
        args[DBMessage.status] = status.rawValue
        args[DBMessage.blocked] = blocked
        args[DBMessage.saved] = _saved
        args[DBMessage.likeCount] = likeCount
        args[DBMessage.liked] = _liked
        args[DBMessage.spamCount] = spamCount
        args[DBMessage.spammed] = _spammed
        args[DBMessage.deleted] = deleted
        args[DBMessage.lastSentAt] = lastSentAt
        args[DBMessage.editedAt] = editedAt

        args[DBMessage.wildcard] = wildcard?.rawData.map { NSKeyedArchiver.archivedData(withRootObject: $0) } ?? NSNull()
        args[DBMessage.forwardedMessage] = forwardedMessage.map { NSKeyedArchiver.archivedData(withRootObject: $0.toJSON()) } ?? NSNull()
    }

    func toData() -> [String: Any] {
        var data = [String: Any]()

        if let text = text, !text.isEmpty {
            data["text"] = text
        }

        if let wildcard = wildcard {
            data["wildcard"] = wildcard.rawData
        }

        if let medias = medias {
            data["media"] = medias.toJSON()
        }

        return data
    }

}
