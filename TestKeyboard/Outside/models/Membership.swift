import Foundation
import ObjectMapper
import FMDB

func < (lhs: Membership, rhs: Membership) -> Bool {
    if AppHelper.instance.environment == .debug {
        let leftIsTest = lhs.channel.name.contains(" Test Group")
        let rightIsTest = rhs.channel.name.contains(" Test Group")

        if leftIsTest && !rightIsTest {
            return false
        } else if !leftIsTest && rightIsTest {
            return true
        }
    }

    let leftSortDate = lhs.sortDate ?? Date(timeIntervalSince1970: 0)
    let rightSortDate = rhs.sortDate ?? Date(timeIntervalSince1970: 0)

    return leftSortDate.compare(rightSortDate) == .orderedDescending
}

func == (lhs: Membership, rhs: Membership) -> Bool {
    return lhs.id == rhs.id
}

class Membership: Model, Comparable {
    enum Role: String {
        case owner = "owner", admin = "admin", member = "member", pending = "pending", noRole = "no_role"
    }

    enum State: String {
        case visible = "visible", hidden = "hidden", ignored = "ignored"
    }

    enum BanType: String {
        case spamBanned = "spam_banned", adminBanned = "admin_banned", noBan = "no_ban"
    }

    var id: Int!
    var role: Role = .noRole
    var username: String?
    var state: State = .visible
    var muted = false

    var banType: BanType = .noBan
    var bannedAt: Date?
    var unbanAt: Date?

    var channel: Channel!
    var user: User?

    var lastMessageSerial: Int?
    var lastMessageCountedSerial: Int?
    var lastMessageText: String?
    var lastMessageMediaType: Media.MediaType?
    var lastMessageUserName: String?
    var lastMessageMine: Bool?
    var lastMessageWildcardType: Wildcard.WildcardType?

    var metadata: MembershipMetadata?
    var sortDate: Date?
    var lastMessage: Message?

    var isDeleted: Bool {
        return role == .noRole && banType == .noBan
    }

    var isJoined: Bool { return role == .owner || role == .admin || role == .member }
    var isAdmin: Bool { return role == .admin }
    var isOwner: Bool { return role == .owner }
    var isPending: Bool { return role == .pending }
    var isVisible: Bool { return state == .visible }
    var isBanned: Bool { return banType != .noBan }

    var banReason: String {
        if banType == .spamBanned {
            return "You're blocked from sending/receiving messages in this group because a message you posted here was marked as spam by 3 or more people in the group. Both group owner and other group admins were notified about it and might unblock you."
        } else if let unbanAt = unbanAt {
            return "You are blocked until \(unbanAt.toShortDateString(thisWeekDateFormat: "EEEE"))"
        } else {
            return "You are blocked permanently in this group."
        }
    }

    var unreadCount: Int {
        if let lastMessageSerial = lastMessageCountedSerial, let metadata = metadata {
            return max(0, lastMessageSerial - metadata.countedSerial)
        }
        return 0
    }

    var smartUsername: String? {
        if let user = user {
            return user.fullName
        }
        return username
    }

    lazy var previewString: NSMutableAttributedString = {
        let attributedString = NSMutableAttributedString()

        var username: String?

        if let mine = self.lastMessageMine , mine {
            username = "You: "
        } else if let lastMessageUserName = self.lastMessageUserName , self.channel.showUser {
            username = "\(lastMessageUserName): "
        }

        if let username = username {
            let usernameString = NSMutableAttributedString(string: username + "\n")
            usernameString.addAttribute(NSForegroundColorAttributeName, value: self.muted ? UIColor(hex: 0x8e8e8e) : UIColor.black, range: NSRange(location: 0, length: username.characters.count))
            attributedString.append(usernameString)
        }

        if let lastMessageMediaType = self.lastMessageMediaType {
            if lastMessageMediaType == .image {
                attributedString.append(AttributedStringCache.instance.previewPhotoIcon)
            } else if lastMessageMediaType == .video {
                attributedString.append(AttributedStringCache.instance.previewVideoIcon)
            }
        }

        var text: String?

        if self.lastMessageText?.isEmpty ?? true, let wildcardType = self.lastMessageWildcardType, wildcardType == .image {
            attributedString.append(AttributedStringCache.instance.previewPhotoIcon)
            text = wildcardType.rawValue
        } else if let lastMessageText = self.lastMessageText {
            text = lastMessageText
        } else if let lastMessageMediaType = self.lastMessageMediaType {
            text = lastMessageMediaType.rawValue
        }

        if let text = text {
            let textString = NSMutableAttributedString(string: text)
            textString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: NSRange(location: 0, length: text.utf16.count))
            attributedString.append(textString)
        }

        return attributedString
    }()

    override init() {
        super.init()
    }

    init(channel: Channel) {
        super.init()
        self.channel = channel
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id          <- map["id"]
        role        <- map["role"]
        username    <- map["username"]
        state       <- map["state"]
        muted       <- map["muted"]

        banType     <- map["ban_type"]
        bannedAt    <- (map["banned_at"], dateTransform)
        unbanAt     <- (map["unban_at"], dateTransform)

        channel     <- map["channel"]
        user        <- map["user"]
    }

    init(resultSet rs: FMResultSet) {
        super.init()

        id = Int(rs.int(forColumn: DBMembership.id))

        if let membershipRole = Role(rawValue: rs.string(forColumn: DBMembership.role)) {
            role = membershipRole
        }
        if let membershipState = State(rawValue: rs.string(forColumn: DBMembership.state)) {
            state = membershipState
        }

        username = stringForColumn(rs, DBMembership.username)
        muted = rs.bool(forColumn: DBMembership.muted)

        if let membershipBanType = BanType(rawValue: rs.string(forColumn: DBMembership.banType)) {
            banType = membershipBanType
        }

        bannedAt = rs.date(forColumn: DBMembership.bannedAt)
        unbanAt = rs.date(forColumn: DBMembership.unbanAt)

        lastMessageSerial = intForColumn(rs, DBMembership.lastMessageSerial)
        lastMessageCountedSerial = intForColumn(rs, DBMembership.lastMessageCountedSerial)
        lastMessageText = stringForColumn(rs, DBMembership.lastMessageText)
        if let rawValue = stringForColumn(rs, DBMembership.lastMessageMediaType) {
            lastMessageMediaType = Media.MediaType(rawValue: rawValue)
        }
        lastMessageUserName = stringForColumn(rs, DBMembership.lastMessageUserName)
        lastMessageMine = rs.bool(forColumn: DBMembership.lastMessageMine)
        if let rawValue = stringForColumn(rs, DBMembership.lastMessageWildcardType) {
            lastMessageWildcardType = Wildcard.WildcardType(rawValue: rawValue)
        }

        sortDate = dateForColumn(rs, DBMembership.sortDate)

        if let metadataString = stringForColumn(rs, DBMembership.metadata) {
            metadata = MembershipMetadata(data: metadataString)
        }

        channel = Channel(resultSet: rs)
    }

    override func bindInsert() {
        args[DBMembership.id] = id
        args[DBMembership.channelId] = channel.id
    }

    override func bindUpdate() {
        args[DBMembership.role] = role.rawValue
        args[DBMembership.username] = username
        args[DBMembership.state] = state.rawValue
        args[DBMembership.muted] = muted

        args[DBMembership.banType] = banType.rawValue
        args[DBMembership.bannedAt] = bannedAt
        args[DBMembership.unbanAt] = unbanAt

        if let lastMessage = lastMessage {
            args = Membership.bindLastMessage(args, message: lastMessage)
        }
        if let sortDate = sortDate {
            args[DBMembership.sortDate] = sortDate
        }
    }

    static func bindLastMessage(_ args: [String: Any], message: Message) -> [String: Any] {
        var args = args

        args[DBMembership.lastMessageSerial] = message.serial
        args[DBMembership.lastMessageCountedSerial] = message.countedSerial
        args[DBMembership.lastMessageText] = message.previewText
        args[DBMembership.lastMessageMediaType] = message.previewMediaType?.rawValue ?? NSNull()
        args[DBMembership.lastMessageUserName] = message.previewUserName ?? NSNull()
        args[DBMembership.lastMessageMine] = message.mine
        if let message = message as? ContentMessage, let wildcard = message.wildcard {
            args[DBMembership.lastMessageWildcardType] = wildcard.type?.rawValue
        }

        return args
    }

    static func getInfoParts(forMemberships memberships: [Membership]) -> [String] {
        let groupsCount = memberships.filter { $0.channel.isGroup }.count
        let directsCount = memberships.filter { $0.channel.isDirect }.count

        var parts = [String]()

        if groupsCount > 0 {
            parts.append("\(groupsCount) \(groupsCount == 1 ? "group" : "groups")")
        }
        if directsCount > 0 {
            parts.append("\(directsCount) \(directsCount == 1 ? "user" : "users")")
        }

        return parts
    }

}
