import Foundation

struct MembershipMetadata {
    var serial: Int
    var countedSerial: Int
    var replySerials: Set<Int>?

    var stringValue: String {
        var parts = ["\(serial),\(countedSerial)"]

        if let replySerials = replySerials {
            parts.append(replySerials.map { String($0) }.joined(separator: ","))
        }

        return parts.joined(separator: "|")
    }

    init?(data: String) {
        let parts = data.components(separatedBy: "|")
        let serials = parts[0].components(separatedBy: ",")

        if let serial = Int(serials[0]), let countedSerial = Int(serials[1]) {
            self.serial = serial
            self.countedSerial = countedSerial
        } else {
            return nil
        }

        if parts.count > 1 {
            let serials = parts[1].components(separatedBy: ",")
            replySerials = Set(serials.flatMap { Int($0) })
        }
    }

    init(serial: Int, countedSerial: Int, replySerials: Set<Int>? = nil) {
        self.serial = serial
        self.countedSerial = countedSerial
        self.replySerials = replySerials
    }

    func hasSameRepliesWith(_ metadata: MembershipMetadata) -> Bool {
        if replySerials == nil && metadata.replySerials == nil {
            return true
        }

        if let serials = replySerials, let metadataSerials = metadata.replySerials {
            return serials.sorted() == metadataSerials.sorted()
        }

        return false
    }

}
