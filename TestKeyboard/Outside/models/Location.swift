import Foundation
import ObjectMapper
import GooglePlaces

struct Location: Mappable {

    var name: String!
    var latitude: Double!
    var longitude: Double!
    var countryCode: String?

    init(place: GMSPlace, countryCode: String) {
        name = place.formattedAddress
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        self.countryCode = countryCode
    }

    init(place: [String: String]) {
        let nameParts = [place["city"], place["country_name"]]
        name = nameParts.flatMap { return ($0 ?? "").isEmpty ? nil : $0 }.joined(separator: ", ")
        if let latitude = place["latitude"], let longitude = place["longitude"] {
            self.latitude = Double(latitude) ?? 0.0
            self.longitude = Double(longitude) ?? 0.0
        }
        countryCode = place["country_code"]
    }

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        name        <- map["name"]
        latitude    <- map["latitude"]
        longitude   <- map["longitude"]
        countryCode <- map["country_code"]
    }

}
