import Foundation
import ObjectMapper
import FMDB

class LoggedInDevice: Model {

    var id: Int!
    var platform: String?
    var appVersion: String?
    var country: String?
    var ipAddress: String?
    var model: String?
    var osVersion: String?
    var userAgent: String?
    var lastDisconnectedAt: Date?
    var online: Bool = false
    var currentDevice: Bool = false

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id                      <- map["id"]
        platform                <- map["platform"]
        appVersion              <- map["app_version"]
        country                 <- map["country"]
        ipAddress               <- map["ip_address"]
        model                   <- map["model"]
        osVersion               <- map["os_version"]
        userAgent               <- map["user_agent"]
        lastDisconnectedAt      <- (map["last_disconnected_at"], dateTransform)
        online                  <- map["online"]
        currentDevice           <- map["current_device"]
    }

}