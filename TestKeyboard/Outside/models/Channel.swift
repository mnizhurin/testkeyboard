import Foundation
import ObjectMapper
import FMDB
import YYText

class Channel: Model, Comparable, AvatarUploadable {

    public static func < (lhs: Channel, rhs: Channel) -> Bool {
        return lhs.smartName < rhs.smartName
    }

    public static func == (lhs: Channel, rhs: Channel) -> Bool {
        return lhs.id == rhs.id
    }

    var avatarCacheKey: String { return  "channel_avatar_\(id!)"}

    enum ChannelType: String {
        case group = "Group", direct = "Direct"
    }

    enum Privacy: String {
        case isPublic = "_public", isPrivate = "_private", isAnonymous = "_anonymous", isNotificationCenter = "_notification_center"
    }

    var id: Int!
    var type: ChannelType = .group

    var name: String = ""
    var channelDescription: String?
    var picture: String?
    var pictureThumb: String?

    var readOnly = false
    var privacy: Privacy = .isPublic
    var deepLinkUrl: String?

    var searchable: Bool = true
    var ageRestricted: Bool = false
    var local: Bool = false
    var location: Location?

    var mediaCount = 0
    var membersCount = 0
    var reportsCount = 0
    var requestsCount = 0
    var bansCount = 0
    var adminsCount = 0

    var managementUpdatedAt: Date?

    var syncedAt: Date!

    var user: User?

    var isGroup: Bool { return type == .group }
    var isDirect: Bool { return type == .direct }

    var isPublic: Bool { return privacy == .isPublic }
    var isPrivate: Bool { return privacy == .isPrivate }
    var isAnonymous: Bool { return privacy == .isAnonymous }
    var isNotificationCenter: Bool { return privacy == .isNotificationCenter }

    var showUser: Bool { return isGroup && !isNotificationCenter }

    var smartName: String { return isDirect ? user!.fullName : name }
    var smartUrl: String? { return isDirect ? user!.avatarThumb : pictureThumb }
    var idForColor: Int { return isDirect ? user!.id : id }

    var smartDescription: String? {
        var parts = [String]()

        if ageRestricted {
            parts.append("🔞 Age Restricted")
        }

        if let channelDescription = channelDescription {
            parts.append(channelDescription)
        }

        return parts.isEmpty ? nil : parts.joined(separator: "\n\n")
    }

    lazy var attributedName: NSMutableAttributedString = {
        let attributedName = NSMutableAttributedString(string: self.smartName)

        if self.isPrivate {
            attributedName.insert(AttributedStringCache.instance.lockIcon, at: 0)
        } else if self.isAnonymous {
            attributedName.insert(AttributedStringCache.instance.anonymousIcon, at: 0)
        }

        return attributedName
    }()

    override init() {
        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id                  <- map["id"]
        type                <- map["type"]

        name                <- map["title"]
        channelDescription  <- map["description"]
        picture             <- map["picture"]
        pictureThumb        <- map["picture_thumb"]

        searchable          <- map["searchable"]
        ageRestricted       <- map["age_restricted"]
        local               <- map["local"]
        location            <- map["location"]

        readOnly            <- map["read_only"]
        privacy             <- map["privacy"]
        deepLinkUrl         <- map["deeplink_url"]

        mediaCount          <- map["media_count"]
        membersCount        <- map["members_count"]
        reportsCount        <- map["management.spammers_count"]
        requestsCount       <- map["management.requests_count"]
        bansCount           <- map["management.bans_count"]
        adminsCount         <- map["management.admins_count"]

        managementUpdatedAt <- (map["management.updated_at"], dateTransform)

        user                <- map["user"]
    }

    init(resultSet rs: FMResultSet) {
        super.init()

        id = Int(rs.int(forColumn: DBChannel.id))
        type = ChannelType(rawValue: rs.string(forColumn: DBChannel.type))!

        readOnly = rs.bool(forColumn: DBChannel.readOnly)
        privacy = Privacy(rawValue: rs.string(forColumn: DBChannel.privacy))!
        deleted = rs.bool(forColumn: DBChannel.deleted)
        deepLinkUrl = rs.string(forColumn: DBChannel.deepLinkUrl)

        searchable = rs.bool(forColumn: DBChannel.searchable)
        ageRestricted = rs.bool(forColumn: DBChannel.ageRestricted)
        local = rs.bool(forColumn: DBChannel.local)

        if let locationData = rs.data(forColumn: DBChannel.location) {
            location = Mapper<Location>().map(JSONObject: NSKeyedUnarchiver.unarchiveObject(with: locationData))
        }

        mediaCount = Int(rs.int(forColumn: DBChannel.mediaCount))
        membersCount = Int(rs.int(forColumn: DBChannel.membersCount))

        syncedAt = rs.date(forColumn: DBChannel.syncedAt)

        if isGroup {
            name = rs.string(forColumn: DBChannel.name)
            channelDescription = rs.string(forColumn: DBChannel.channelDescription)
            picture = rs.string(forColumn: DBChannel.picture)
            pictureThumb = rs.string(forColumn: DBChannel.pictureThumb)

            reportsCount = Int(rs.int(forColumn: DBChannel.reportsCount))
            requestsCount = Int(rs.int(forColumn: DBChannel.requestsCount))
            adminsCount = Int(rs.int(forColumn: DBChannel.adminsCount))
            bansCount = Int(rs.int(forColumn: DBChannel.bansCount))
        } else if isDirect {
            let user = User()
            user.id = Int(rs.int(forColumn: DBChannel.userId))
            user.firstName = rs.string(forColumn: DBChannel.userFirstName)
            user.lastName = rs.string(forColumn: DBChannel.userLastName)
            user.avatarThumb = rs.string(forColumn: DBChannel.userAvatarThumb)
            self.user = user
        }
    }

    override func bindInsert() {
        args[DBChannel.id] = id
        args[DBChannel.type] = type.rawValue
        args[DBChannel.syncedAt] = Date()
    }

    override func bindUpdate() {
        args[DBChannel.readOnly] = readOnly
        args[DBChannel.privacy] = privacy.rawValue
        args[DBChannel.deleted] = deleted
        args[DBChannel.deepLinkUrl] = deepLinkUrl

        args[DBChannel.searchable] = searchable
        args[DBChannel.ageRestricted] = ageRestricted
        args[DBChannel.local] = local
        args[DBChannel.location] = location.map { NSKeyedArchiver.archivedData(withRootObject: $0.toJSON()) } ?? NSNull()

        args[DBChannel.mediaCount] = mediaCount
        args[DBChannel.membersCount] = membersCount

        if isGroup {
            args[DBChannel.name] = name
            args[DBChannel.channelDescription] = channelDescription ?? NSNull()
            args[DBChannel.picture] = picture ?? NSNull()
            args[DBChannel.pictureThumb] = pictureThumb ?? NSNull()
            args[DBChannel.reportsCount] = reportsCount
            args[DBChannel.requestsCount] = requestsCount
            args[DBChannel.adminsCount] = adminsCount
            args[DBChannel.bansCount] = bansCount
        } else if let user = user {
            args[DBChannel.userId] = user.id
            args[DBChannel.userFirstName] = user.firstName
            args[DBChannel.userLastName] = user.lastName ?? NSNull()
            args[DBChannel.userAvatarThumb] = user.avatarThumb ?? NSNull()
        }
    }

    class func initialAvatarCacheKey(_ channelName: String) -> String {
        return "channel_avatar_\(channelName)"
    }

}
