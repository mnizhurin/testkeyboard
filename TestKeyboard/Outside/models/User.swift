import Foundation
import ObjectMapper

class User: Model, Comparable, AvatarUploadable {

    public static func < (lhs: User, rhs: User) -> Bool {
        return lhs.fullName < rhs.fullName
    }

    public static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }

    var avatarCacheKey: String { return  "user_avatar_\(id!)"}

    var id: Int!

    var firstName: String!
    var lastName: String?

    var muted: Bool!
    var pushNotificationSound: String?

    var avatar: String?
    var avatarThumb: String?
    var profileUrl: String!

    var groupsCount: Int?
    var messagesCount: Int?
    var savesCount: Int?
    var likesCount: Int?
    var bansCount: Int?

    var online = false
    var lastSeenDate: Date?

    var membershipsUpdatedAt: Date?
    var savesUpdatedAt: Date?
    var actionSerial: Int?

    var badges = [UserBadge]()

    var fullName: String {
        var fullName = ""
        if let firstName = firstName {
            fullName += firstName
        }
        if let lastName = lastName, !lastName.isEmpty {
            fullName += " " + lastName
        }
        return fullName
    }

    override init() {
        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id                      <- map["id"]

        firstName               <- map["first_name"]
        lastName                <- map["last_name"]

        muted                   <- map["muted"]
        pushNotificationSound   <- map["push_notification_sound"]

        avatar                  <- map["avatar"]
        avatarThumb             <- map["avatar_thumb"]
        profileUrl              <- map["deeplink_url"]

        groupsCount             <- map["groups_count"]
        messagesCount           <- map["messages_count"]
        savesCount              <- map["others_bookmarks_count"]
        likesCount              <- map["others_likes_count"]
        bansCount               <- map["bans_count"]

        online                  <- map["online"]
        lastSeenDate            <- (map["last_disconnected_at"], dateTransform)

        membershipsUpdatedAt    <- (map["memberships_updated_at"], dateTransform)
        savesUpdatedAt          <- (map["bookmarks_updated_at"], dateTransform)
        actionSerial            <- map["action_serial"]

        badges                  <- map["badges"]

        if deleted {
            firstName = "Deleted User"
            lastName = nil
            avatarThumb = nil
        }
    }

    func parametersForRequest() -> [String: AnyObject] {
        var params = [String: AnyObject]()

        params["first_name"] = firstName as AnyObject?
        params["last_name"] = lastName as AnyObject?

        if let avatar = avatar {
            params["avatar"] = avatar as AnyObject?
        }
        if let avatarThumb = avatarThumb {
            params["avatar_thumb"] = avatarThumb as AnyObject?
        }

        return params
    }

    func badgeLevel(forKind kind: UserBadge.Kind) -> Int {
        for badge in badges {
            if badge.kind == kind {
                return badge.level
            }
        }
        return 0
    }

}
