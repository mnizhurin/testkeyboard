import Foundation
import ObjectMapper
import FMDB

class EventMessage: DataMessage {
    enum EventType: String {
        case ChannelCreated = "channel_created"
        case ChannelDeleted = "channel_deleted"
        case ChannelChanged = "channel_changed"
        case UserJoined = "user_joined"
        case UserLeft = "user_left"
        case UserBanned = "user_banned"
        case UserUnbanned = "user_unbanned"
        case UserChanged = "user_changed"
        case UserDeleted = "user_deleted"
        case None = "none"
    }

    lazy var type: EventType = {
        if let name = self.data["name"] as? String, let eventType = EventType(rawValue: name) {
            return eventType
        }
        return .None
    }()

    var shouldUpdateChannel: Bool {
        return type == .ChannelChanged || type == .ChannelDeleted || type == .UserJoined || type == .UserLeft || type == .UserDeleted
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override init(resultSet rs: FMResultSet) {
        super.init(resultSet: rs)
    }

    override var previewText: String {
        return text
    }

    var text: String {
        switch type {
            case .ChannelCreated:
                if let name = parseName() {
                    return "\(name) created this group"
                }
            case .ChannelDeleted:
                if let deletedBy = data["deleted_by"] as? String {
                    if deletedBy == "admin" {
                        return "Group owner deleted this group"
                    } else if deletedBy == "system" {
                        return "This group was deleted by Grouvi due to inactivity"
                    }
                }
            case .ChannelChanged:
                if let attribute = data["attribute"] as? String {
                    let oldValue = data["old_value"] as? String
                    let newValue = data["new_value"] as? String

                    switch attribute {
                        case "title":
                            return "Group owner changed group name from \(oldValue ?? "") to \(newValue ?? "")"
                        case "picture":
                            return "Group owner changed group icon"
                        case "description":
                            return "Group owner changed group description"
                        case "privacy":
                            if let newValue = newValue, let privacy = Channel.Privacy(rawValue: newValue) {
                                return "Group owner changed group visibility to \(privacy == .isPrivate ? "private" : "public")"
                            }
                        case "age_restricted":
                            if let newValue = data["new_value"] as? Bool {
                                return newValue ? "Group owner added Age Restriction (18+) in group settings" : "Group owner removed Age Restriction (18+) in group settings"
                            }
                        case "local":
                            if let newValue = data["new_value"] as? Bool {
                                if newValue {
                                    if let location = Mapper<Location>().map(JSONObject: data["location"]), let locationName = location.name {
                                       return "Group owner made group local. This group now available only to Grouvi members in '\(locationName)'."
                                    }
                                } else {
                                    return "This group no longer a local group. It's now available to all Grouvi members, regardless of location."
                                }
                            }
                        case "location":
                            if let newLocation = Mapper<Location>().map(JSONObject: data["new_value"]), let oldLocation = Mapper<Location>().map(JSONObject: data["old_value"]),
                               let newLocationName = newLocation.name, let oldLocationName = oldLocation.name {
                                return "Group owner changed group locality from '\(oldLocationName)' to '\(newLocationName)'"
                            }
                    default: ()
                    }
                }
            case .UserJoined:
                if let name = parseName() {
                    return "\(name) joined"
                }
            case .UserLeft:
                if let name = parseName() {
                    return "\(name) left"
                }
            case .UserBanned:
                if let membership = Mapper<Membership>().map(JSONObject: data["membership"]), let name = membership.smartUsername {
                    if membership.banType == .spamBanned {
                        return "\(name) temporarily blocked for spamming. Pending review by a group owner"
                    } else if membership.banType == .adminBanned, let jsonObject = data["membership"]?["banner_membership"], let bannerMembership = Mapper<Membership>().map(JSONObject: jsonObject), let bannerName = bannerMembership.smartUsername {
                        let roleString = bannerMembership.isOwner ? "Group owner" : "Group admin"
                        let periodString = membership.unbanAt == nil ? "permanently" : "until \(membership.unbanAt!.toDateOnlyString())"
                        return "\(roleString) \(bannerName) blocked \(name) \(periodString)"
                    }
                }
            case .UserUnbanned:
                if let membership = Mapper<Membership>().map(JSONObject: data["membership"]), let name = membership.smartUsername {
                    if let jsonObject = data["membership"]?["unbanner_membership"], let unbannerMembership = Mapper<Membership>().map(JSONObject: jsonObject), let unbannerName = unbannerMembership.smartUsername {
                        let roleString = unbannerMembership.isOwner ? "Group owner" : "Group admin"
                        return "\(roleString) \(unbannerName) unblocked \(name)"
                    } else {
                        return "\(name)'s block period has expired"
                    }
                }
            case .UserChanged:
                if let attribute = data["attribute"] as? String, let oldValue = (data["old_value"] as? [String])?.joined(separator: " "), let newValue = (data["new_value"] as? [String])?.joined(separator: " ") {
                    switch attribute {
                    case "name":
                        return "User changed name from \(oldValue) to \(newValue)"
                    default: ()
                    }
                }
            case .UserDeleted:
                if let name = parseName() {
                    return "\(name) deleted account"
                }
            default: ()
        }

        print(data)
        return type.rawValue
    }

    func parseName() -> String? {
        if let userData = data["user"], let user = Mapper<User>().map(JSONObject: userData) {
            return user.fullName
        } else if let membershipData = data["membership"], let membership = Mapper<Membership>().map(JSONObject: membershipData), let username = membership.username {
            return username
        }
        return nil
    }

    func parseChannel() -> Channel? {
        return Mapper<Channel>().map(JSONObject: data["channel"])
    }

}
