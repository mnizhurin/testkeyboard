import Foundation
import ObjectMapper

class DeepLinkModel: Model {
    var channel: Channel?
    var targetSerial: Int?

    var user: User?

    override func mapping(map: Map) {
        super.mapping(map: map)

        channel <- map["channel"]
        targetSerial <- map["message_serial"]

        user <- map["user"]
    }

}
