import Foundation
import ObjectMapper

class VideoWildcard: LinkWildcard {

    public enum ActionType { case copy, forward }

    var width: Int = 200
    var height: Int = 200
    var source: String?
    var embedSource: String?
    var duration: Int = 0
    var action: ((ActionType) -> ())?

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        width           <- map["embed_width"]
        height          <- map["embed_height"]
        source          <- map["source"]
        embedSource     <- map["embed_src"]
        duration        <- map["duration"]
    }

    override func handleAction(navigationController: UINavigationController?) {

        var webView: AlertWebView? = AlertWebView(frame: CGRect(x: 0, y: 0, width: width, height: height))

        let alertController = AlertController(view: webView, style: .actionSheet)
        alertController.buttonTintColor = UIColor(hex: 0x1F1F21)
        alertController.buttonBackgroundColor = UIColor(hex: 0xbdbec2)
        alertController.dividerColor = UIColor(hex: 0x8e8e93)

        alertController.configContentView = { [unowned self] view in
            if let view = view as? AlertWebView {
                let proportion = CGFloat(self.height) / CGFloat(self.width)
                view.frame.size.height = view.frame.size.width * proportion

                view.set(wildCard: self)
            }
        }
        alertController.dismissAlertController = {
            webView?.loadHTMLString("", baseURL: nil)
            webView = nil
        }

        alertController.addAction(AlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(AlertAction(title: "Forward", style: .default, handler: { [unowned self] _ in self.action?(.forward) }))

        let copyAction = AlertAction(title: "Copy Link", style: .default, handler: { [unowned self] alertAction in
            alertAction?.title = "Link Copied"
            alertAction?.enabled = false
            self.action?(.copy)
        })
        copyAction.dismissesAlert = false
        alertController.addAction(copyAction)

        alertController.needSmallView = { [unowned alertController] needLess in
            if copyAction.isHidden != needLess {
                copyAction.isHidden = needLess
                alertController.viewDidLayoutSubviews()
            }
        }
        alertController.show()
    }

}
