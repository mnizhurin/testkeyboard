import Foundation
import ObjectMapper

class InternalWildcard: ServiceWildcard {

    enum Page: String {
        case userGuide = "faq"
        case activeSessions = "active_sessions"
    }

    override var image: UIImage? {
        return ImageCache.instance.serviceAppIcon
    }

    override var title: String {
        return "Grouvi"
    }

    override var subtitle: String {
        if page == .userGuide {
            return "User's Guide"
        } else if page == .activeSessions {
            return "Logged in Devices"
        }
        return ""
    }

    var page: Page?

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        page    <- map["page"]
    }

    override func handleAction(navigationController: UINavigationController?) {
        if page == .userGuide {
            let controller = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UsersGuide")
            navigationController?.pushViewController(controller, animated: true)
        } else if page == .activeSessions {
            let controller = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ActiveSessions")
            navigationController?.pushViewController(controller, animated: true)
        }
    }

}
