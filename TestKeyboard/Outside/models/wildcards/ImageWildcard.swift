import Foundation
import ObjectMapper

class ImageWildcard: Wildcard {

    var url: String!
    var thumbUrl: String?
    var width: Int = 100
    var height: Int = 100
    var size: Int?

    var media: Media {
        let media = Media(type: .image)
        media.url = url
        media.thumbUrl = thumbUrl ?? url
        media.width = width
        media.height = height
        media.size = size
        return media
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        url         <- map["url"]
        thumbUrl    <- map["thumb_url"]
        width       <- map["width"]
        height      <- map["height"]
        size        <- map["size"]
    }

}
