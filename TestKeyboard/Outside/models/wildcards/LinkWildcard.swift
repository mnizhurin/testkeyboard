import Foundation
import ObjectMapper

class LinkWildcard: Wildcard {

    var url: String!
    var siteName: String!
    var title: String!
    var siteDescription: String?
    var imageUrl: String?

    init(url: String) {
        super.init()

        self.url = url
        self.rawData = ["url": url]
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        url             <- map["url"]
        siteName        <- map["site_name"]
        title           <- map["title"]
        siteDescription <- map["description"]
        imageUrl        <- map["image_url"]
    }

    override func handleAction(navigationController: UINavigationController?) {
        if let url = URL(string: url), let controller = navigationController {
            UrlHelper.instance.smartOpenUrl(url: url, controller: controller)
        }
    }

}
