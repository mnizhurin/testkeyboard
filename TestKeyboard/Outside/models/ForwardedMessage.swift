import Foundation
import ObjectMapper

class ForwardedMessage: Model {
    var id: Int!
    var serial: Int!
    var countedSerial: Int!
    var channel: Channel?

    init(message: ContentMessage, channel: Channel) {
        super.init()
        id = message.id
        serial = message.serial
        countedSerial = message.countedSerial
        self.channel = channel
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id              <- map["id"]
        serial          <- map["serial"]
        countedSerial   <- map["counted_serial"]
        channel         <- map["channel"]
    }

}
