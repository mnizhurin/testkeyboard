import Foundation
import ObjectMapper
import FMDB

class SavesChannel: Channel {

    var messagesCount: Int = 0

    required init?(map: Map) {
        super.init(map: map)
    }

    init(channel: Channel, messagesCount: Int) {
        super.init()

        id = channel.id
        type = channel.type
        privacy = channel.privacy

        if channel.isGroup {
            name = channel.name
            pictureThumb = channel.pictureThumb
        } else {
            user = channel.user
        }

        self.messagesCount = messagesCount
    }

    override init(resultSet rs: FMResultSet) {
        super.init()

        id = Int(rs.int(forColumn: DBSavesChannel.id))
        type = ChannelType(rawValue: rs.string(forColumn: DBSavesChannel.type))!
        messagesCount = Int(rs.int(forColumn: DBSavesChannel.messagesCount))
        privacy = Privacy(rawValue: rs.string(forColumn: DBChannel.privacy))!

        if isGroup {
            name = rs.string(forColumn: DBSavesChannel.name)
            pictureThumb = rs.string(forColumn: DBSavesChannel.pictureThumb)
        } else if isDirect {
            let user = User()
            user.id = Int(rs.int(forColumn: DBSavesChannel.userId))
            user.firstName = rs.string(forColumn: DBSavesChannel.userFirstName)
            user.lastName = rs.string(forColumn: DBSavesChannel.userLastName)
            user.avatarThumb = rs.string(forColumn: DBSavesChannel.userAvatarThumb)
            self.user = user
        }
    }

    override func bindInsert() {
        args[DBSavesChannel.id] = id
        args[DBSavesChannel.type] = type.rawValue
    }

    override func bindUpdate() {
        args[DBSavesChannel.privacy] = privacy.rawValue
        args[DBSavesChannel.messagesCount] = messagesCount

        if isGroup {
            args[DBSavesChannel.name] = name
            args[DBSavesChannel.pictureThumb] = pictureThumb
        } else if let user = user {
            args[DBSavesChannel.userId] = user.id
            args[DBSavesChannel.userFirstName] = user.firstName
            args[DBSavesChannel.userLastName] = user.lastName
            args[DBSavesChannel.userAvatarThumb] = user.avatarThumb
        }
    }

}
