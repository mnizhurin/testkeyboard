
protocol AvatarUploadable {

    var avatarCacheKey: String { get }

}
