import Foundation
import UIKit

protocol PseudoAccessoryViewDelegate: class {
    func pseudoAccessoryView(_ pseudoAccessoryView: PseudoAccessoryView, keyboardFrameDidChange frame: CGRect)
}

class PseudoAccessoryView: UIView {
    private let keyPathSelector = "center"

    weak var delegate: PseudoAccessoryViewDelegate?

    private var heightConstraint: NSLayoutConstraint?

    var height: CGFloat = 0 {
        didSet {
            heightConstraint?.constant = height
        }
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)

        superview?.removeObserver(self, forKeyPath: keyPathSelector)
        newSuperview?.addObserver(self, forKeyPath: keyPathSelector, context: nil)
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        var heightConstraint: NSLayoutConstraint?

        for constraint in constraints {
            if constraint.firstItem as? UIView == self, constraint.firstAttribute == .height, constraint.relation == .equal {
                heightConstraint = constraint
                break
            }
        }

        self.heightConstraint = heightConstraint
        self.heightConstraint?.constant = height
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let superview = superview, object as? UIView == superview, keyPath == keyPathSelector {
            let keyboardFrame = superview.convert(superview.bounds, to: nil)
            delegate?.pseudoAccessoryView(self, keyboardFrameDidChange: keyboardFrame)
        }
    }

    deinit {
        superview?.removeObserver(self, forKeyPath: keyPathSelector)
    }

}
