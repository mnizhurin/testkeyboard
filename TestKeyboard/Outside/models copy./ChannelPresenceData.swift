import Foundation
import ObjectMapper

class ChannelPresenceData: Model {

    var channelId: Int!
    var onlineCount: Int?
    var online: Bool?
    var lastSeen: Date?

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        channelId       <- map["channel_id"]
        onlineCount     <- map["online_members_count"]
        online          <- map["online"]
        lastSeen        <- (map["last_disconnected_at"], dateTransform)
    }

}
