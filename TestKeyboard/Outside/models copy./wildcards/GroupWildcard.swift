import Foundation
import ObjectMapper
import YYText

class GroupWildcard: ServiceWildcard {

    enum Page: String {
        case _default = "default", groupInfo = "info"
    }

    override var hasAvatar: Bool {
        return group != nil
    }

    override var title: String {
        return group?.name ?? "Unknown Group"
    }

    func attributedTitle(isYYText: Bool = false) -> NSAttributedString {
        var attributedName: NSMutableAttributedString = NSMutableAttributedString()
        if let group = group {
            switch (group.privacy, isYYText) {
            	case (.isAnonymous, true), (.isPrivate, true):
                    let image = group.isPrivate ? ImageCache.instance.lockIcon : ImageCache.instance.anonymousIcon
                	attributedName = NSMutableAttributedString.yy_attachmentString(withContent: image, contentMode:UIViewContentMode.center, attachmentSize: image.size, alignTo: WildcardTheme.titleFont, alignment: YYTextVerticalAlignment.center )
            		attributedName.yy_appendString(" ")
                    break
            	case (.isAnonymous, false), (.isPrivate, false):
                    attributedName.insert(group.isPrivate ? AttributedStringCache.instance.lockIcon : AttributedStringCache.instance.anonymousIcon, at: 0)
            	default: break
            }
        }
        attributedName.append(NSAttributedString(string: title))

        return attributedName
    }

    override var subtitle: String {
        if let group = group {
            return "\(group.membersCount) \(group.membersCount == 1 ? "member" : "members")"
        } else {
            return ""
        }
    }

    var group: Channel?
    var page: Page = ._default

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        group   <- map["group"]
        group?.type = .group

        page    <- map["page"]
    }

    override func handleAction(navigationController: UINavigationController?) {
        if let group = group {
            if page == .groupInfo {
                let groupInfoController = UIStoryboard(name: "GroupInfo", bundle: nil).instantiateViewController(withIdentifier: "GroupInfo") as! GroupInfoController
                groupInfoController.bind(group.id)
                navigationController?.pushViewController(groupInfoController, animated: true)
            } else {
                let groupChatController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChat") as! GroupChatController
                groupChatController.bind(channelId: group.id)
                navigationController?.pushViewController(groupChatController, animated: true)
            }
        }
    }

}
