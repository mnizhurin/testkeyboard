import Foundation
import ObjectMapper

class MessageWildcard: GroupWildcard {

    var message: Message?

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        message     <- map["message"]
    }

    override func handleAction(navigationController: UINavigationController?) {
        if let group = group, let message = message {
            navigationController?.pushViewController(Router.groupChatController(channelId: group.id, targetSerial: message.serial), animated: true)
        }
    }

}
