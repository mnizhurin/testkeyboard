import Foundation
import ObjectMapper

class UserWildcard: ServiceWildcard {

    override var hasAvatar: Bool {
        return user != nil
    }

    override var title: String {
        return user?.fullName ?? "Unknown User"
    }

    override var subtitle: String {
        let groupsCount = user?.groupsCount ?? 0
        return "\(groupsCount) \(groupsCount == 1 ? "group" : "groups")"
    }

    var user: User?

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        user <- map["user"]
    }

    override func handleAction(navigationController: UINavigationController?) {
        if let user = user {
            let profileController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "UserProfileController") as! ProfileController
            profileController.bind(user.id)
            navigationController?.pushViewController(profileController, animated: true)
        }
    }

}
