import Foundation
import ObjectMapper

class ServiceWildcard: Wildcard {

    var hasAvatar: Bool { return false }
    var image: UIImage? { return nil }
    var title: String { return "Service Wildcard Title" }
    var subtitle: String { return "Service Wildcard Subtitle" }

}
