import Foundation
import ObjectMapper

class AppStoreWildcard: ServiceWildcard {

    override var image: UIImage? {
        return ImageCache.instance.appStoreIcon
    }

    override var title: String {
        return "App Store"
    }

    override var subtitle: String {
        return "Rate our app"
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func handleAction(navigationController: UINavigationController?) {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1003701354") {
            UIApplication.shared.openURL(url)
        }
    }

}
