import Foundation
import ObjectMapper
import FMDB

class Wildcard: Mappable {

    enum WildcardType: String {
        case group = "group"
        case user = "user"
        case message = "message"
        case _internal = "internal"
        case grouviApp = "grouvi_app"
        case link = "link"
        case image = "image"
    }

    var type: WildcardType?
    var rawData: Any?

    init() {
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        type <- map["type"]
    }

    func handleAction(navigationController: UINavigationController?) {
    }

    static func map(data: Any?) -> Wildcard? {
        var wildcard: Wildcard?

        if let data = data as? [String: Any], let typeRawValue = data["type"] as? String, let type = WildcardType(rawValue: typeRawValue) {
            switch type {
                case .group: wildcard = Mapper<GroupWildcard>().map(JSONObject: data)
                case .user: wildcard = Mapper<UserWildcard>().map(JSONObject: data)
                case .message: wildcard = Mapper<MessageWildcard>().map(JSONObject: data)
                case ._internal: wildcard = Mapper<InternalWildcard>().map(JSONObject: data)
                case .grouviApp: wildcard = Mapper<AppStoreWildcard>().map(JSONObject: data)
                case .image: wildcard = Mapper<ImageWildcard>().map(JSONObject: data)
                case .link:
                    if let subtype = data["subtype"] as? String, subtype == "video" {
                        wildcard = Mapper<VideoWildcard>().map(JSONObject: data)
                    } else {
                        wildcard = Mapper<LinkWildcard>().map(JSONObject: data)
                    }
            }
        }

        wildcard?.rawData = data

        return wildcard
    }

}
