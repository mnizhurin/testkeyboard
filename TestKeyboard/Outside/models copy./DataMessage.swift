import Foundation
import ObjectMapper
import FMDB

class DataMessage: Message {

    var data: [String: AnyObject]!

    override init() {
        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        data <- map["data"]
    }

    override init(resultSet rs: FMResultSet) {
        super.init(resultSet: rs)

        if let dataData = rs.data(forColumn: DBMessage.data) {
            data = NSKeyedUnarchiver.unarchiveObject(with: dataData) as! [String: AnyObject]
        }
    }

    override func bindUpdate() {
        super.bindUpdate()
        args[DBMessage.data] = NSKeyedArchiver.archivedData(withRootObject: data) as AnyObject?
    }

}
