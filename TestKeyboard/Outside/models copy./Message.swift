import Foundation
import ObjectMapper
import FMDB
private func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

private func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


func < (lhs: Message, rhs: Message) -> Bool {
    if lhs.serial == rhs.serial {
        if lhs.id == rhs.id {
            return lhs.createdAt!.compare(rhs.createdAt! as Date) == .orderedAscending
        } else {
            return lhs.id > rhs.id
        }
    } else {
        return lhs.serial < rhs.serial
    }
}

func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.uuid == rhs.uuid
}

class Message: Model, Comparable {
    var uuid: String!

    var id: Int?
    var channelId: Int!
    var serial: Int!
    var countedSerial: Int!

    var mine: Bool {
        return false
    }

    var previewText: String {
        return ""
    }
    var previewUserName: String? {
        return nil
    }
    var previewMediaType: Media.MediaType? {
        return nil
    }

    var _createdAtWithoutTime: Date?
    var createdAtWithoutTime: Date? {
        get {
            if let createdAtWithoutTime = _createdAtWithoutTime {
                return createdAtWithoutTime
            } else {
                _createdAtWithoutTime = createdAt?.withoutTime() as Date?
                return _createdAtWithoutTime
            }
        }
    }

    var _dateHeaderText: String?
    var dateHeaderText: String? {
        get {
            if let dateHeaderText = _dateHeaderText {
                return dateHeaderText
            } else {
                _dateHeaderText = NSDate.stringForDisplay(from: createdAt as Date!, prefixed: false, alwaysDisplayTime: false)
                return _dateHeaderText
            }
        }
    }

    var debugInfo: String {
        let shortUuid = String(uuid.characters.prefix(2)) + ".." + String(uuid.characters.suffix(2))
        return "id: \(id ?? 0), serial: \(serial!), counted: \(countedSerial!), uuid: \(shortUuid)"
    }

    override init() {
        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        uuid                <- map["uuid"]

        id                  <- map["id"]
        channelId           <- map["channel_id"]
        serial              <- map["serial"]
        countedSerial       <- map["counted_serial"]
    }

    init(resultSet rs: FMResultSet) {
        super.init()

        uuid = rs.string(forColumn: DBMessage.uuid)

        id = Int(rs.int(forColumn: DBMessage.id))
        channelId = Int(rs.int(forColumn: DBMessage.channelId))
        serial = Int(rs.int(forColumn: DBMessage.serial))
        countedSerial = Int(rs.int(forColumn: DBMessage.countedSerial))

        createdAt = rs.date(forColumn: DBMessage.createdAt)
        updatedAt = rs.date(forColumn: DBMessage.updatedAt)
    }

    override func bindInsert() {
        args[DBMessage.uuid] = uuid
        args[DBMessage.channelId] = channelId
        args[DBMessage.type] = String(describing: self).components(separatedBy: ".").last! // TODO: this is hack. Need to find a correct way to get the class name without package name
    }

    override func bindUpdate() {
        args[DBMessage.id] = id
        args[DBMessage.serial] = serial
        args[DBMessage.countedSerial] = countedSerial
        args[DBMessage.createdAt] = createdAt
        args[DBMessage.updatedAt] = updatedAt
    }

    // static methods

    static func map(_ data: Any?) -> Message {
        if let data = data as? [String: Any], let type = data["type"] as? String {
            switch type {
                case "UserMessage": return Mapper<ContentMessage>().map(JSONObject: data)!
                case "EventMessage": return Mapper<EventMessage>().map(JSONObject: data)!
                case "NotificationMessage": return Mapper<NotificationMessage>().map(JSONObject: data)!
                default: ()
            }
        }
        return Mapper<Message>().map(JSONObject: data)!
    }

    static func getConsecutiveMessages(_ source: [Message], targetSerial: Int? = nil) -> [Message] {
        var result = [Message]()

        if !source.isEmpty {
            var targetIndex: Int? = nil

            if let targetSerial = targetSerial {
                for (index, message) in source.enumerated() {
                    if message.serial == targetSerial {
                        targetIndex = index
                        break
                    }
                }
            } else {
                targetIndex = 0
            }

            if let targetIndex = targetIndex {
                result.append(source[targetIndex])

                for index in (0..<targetIndex).reversed() {
                    if source[index].serial - source[index + 1].serial <= 1 {
                        result.insert(source[index], at: 0)
                    } else {
                        break
                    }
                }

                for index in (targetIndex + 1)..<source.count {
                    if source[index - 1].serial - source[index].serial <= 1 {
                        result.append(source[index])
                    } else {
                        break
                    }
                }
            }
        }

        return result
    }

    static func serialsLog(_ prefix: String, messages: [Message]) -> String {
        let serials = messages.map { $0.serial! }
        return "\(prefix) | \(messages.count) | \(serials)"
    }

    static func containsSerial(_ messages: [Message], serial: Int) -> Bool {
        for message in messages {
            if message.serial == serial {
                return true
            }
        }
        return false
    }

}
