import Foundation
import ObjectMapper

class Giphy: Mappable {
    var originalUrl: String?
    var thumbUrl: String?
    var width: Int?
    var height: Int?
    var size: Int?

    var smallUrl: String?
    var ratio: Double {
        get {
            if let width = width, let height = height {
                return width > height ? Double(width) / Double(height) : Double(height) / Double(width)
            } else {
                return 1
            }
        }
    }

    var wildcard: Any {
        get {
            var json = [String: Any]()
            json["type"] = Wildcard.WildcardType.image.rawValue
            json["url"] = originalUrl
            json["thumb_url"] = thumbUrl
            json["width"] = width
            json["height"] = height
            json["size"] = size
            json["subtype"] = "gif"
            return json
        }
    }
    static let transformer = TransformOf<Int, String>(fromJSON: { $0.flatMap { Int($0) } }, toJSON: { $0.map { String($0) } })

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        originalUrl     <- map["images.original.url"]
        smallUrl        <- map["images.fixed_height_small.url"]
        thumbUrl        <- map["images.fixed_width_small_still.url"]
        size            <- (map["images.original.size"], Giphy.transformer)

        width           <- (map["images.original.width"], Giphy.transformer)
        height          <- (map["images.original.height"], Giphy.transformer)
    }

}
