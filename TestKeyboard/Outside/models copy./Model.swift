import Foundation
import ObjectMapper
import FMDB

class Model: Mappable {
    var args = [String: Any]()

    var createdAt: Date?
    var updatedAt: Date?
    var deleted: Bool = false

    let dateTransform = TransformOf<Date, String>(fromJSON: { $0?.toServerDate() }, toJSON: { $0?.toServerDateString() })

    init() {
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        createdAt <- (map["created_at"], dateTransform)
        updatedAt <- (map["updated_at"], dateTransform)
        deleted <- map["is_deleted"]
    }

    func stringForColumn(_ rs: FMResultSet, _ column: String) -> String? {
        return rs.columnIsNull(column) ? nil : rs.string(forColumn: column)
    }

    func intForColumn(_ rs: FMResultSet, _ column: String) -> Int? {
        return rs.columnIsNull(column) ? nil : Int(rs.int(forColumn: column))
    }

    func dateForColumn(_ rs: FMResultSet, _ column: String) -> Date? {
        return rs.columnIsNull(column) ? nil : rs.date(forColumn: column)
    }

    func bindInsert() {
    }

    func bindUpdate() {
    }

    func insertArguments() -> [String: Any] {
        args = [:]
        bindInsert()
        bindUpdate()
        return args
    }

    func updateArguments() -> [String: Any] {
        args = [:]
        bindUpdate()
        return args
    }

}
