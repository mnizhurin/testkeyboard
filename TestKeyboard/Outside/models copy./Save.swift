import Foundation
import ObjectMapper

class Save: Model {

    var message: ContentMessage!

    override func mapping(map: Map) {
        super.mapping(map: map)
        message <- map["message"]
    }

}
