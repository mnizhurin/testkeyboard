import Foundation
import ObjectMapper

class UserBadge: Mappable {

    enum Kind: String {
        case friend = "friend", brain = "brain", power = "power", star = "star", mayor = "mayor"
    }

    var kind: Kind!
    var level: Int!

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        kind <- map["kind"]
        level <- map["level"]
    }

}
