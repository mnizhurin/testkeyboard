import Foundation
import ObjectMapper

struct AuthData: Mappable {

    var accessToken: String!
    var deviceId: Int!

    init(accessToken: String, deviceId: Int) {
        self.accessToken = accessToken
        self.deviceId = deviceId
    }

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        accessToken     <- map["access_token"]
        deviceId        <- map["device_id"]
    }

}
