import Foundation
import ObjectMapper
import FMDB

class Media: Model {
    enum MediaType: String {
        case image = "image", video = "video"
    }

    var messageUuid: String!

    var order = 0
    var type: MediaType!

    var url: String!
    var thumbUrl: String!
    var size: Int?
    var width: Int?
    var height: Int?

    var assetUrl: URL?

    var isImage: Bool { return type == .image }
    var isVideo: Bool { return type == .video }
    var isUploaded: Bool { return !url.hasPrefix("local/") }
    var isThumbUploaded: Bool { return !thumbUrl.hasPrefix("local/") }

    lazy var documentsUrl: URL = {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }()

    var localUrl: URL {
        return self.documentsUrl.appendingPathComponent("\(self.type.rawValue)/\(self.url ?? "")")
    }

    var localThumbUrl: URL {
        return self.documentsUrl.appendingPathComponent("\(self.type.rawValue)/\(self.thumbUrl ?? "")")
    }

    var videoThumbUrl: URL {
        return self.documentsUrl.appendingPathComponent("\(self.type.rawValue)/\(self.url ?? "").jpg")
    }

    init(type: MediaType, imageSize: CGSize? = nil, fileSize: Int? = nil) {
        super.init()

        let uuid = NSUUID().uuidString

        self.type = type
        url = "local/\(uuid).\(isImage ? "jpg" : "mp4")"
        thumbUrl = "local/\(uuid)_thumb.jpg"
        size = fileSize

        if let imageSize = imageSize {
            width = Int(imageSize.width)
            height = Int(imageSize.height)
        }
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        type            <- map["type"]
        url             <- map["url"]
        thumbUrl        <- map["thumb_url"]
        size            <- map["size"]
        width           <- map["width"]
        height          <- map["height"]
    }

    init(resultSet rs: FMResultSet) {
        super.init()

        messageUuid = rs.string(forColumn: DBMedia.messageUuid)
        order = Int(rs.int(forColumn: DBMedia.order))
        type = MediaType(rawValue: rs.string(forColumn: DBMedia.type))

        url = rs.string(forColumn: DBMedia.url)
        thumbUrl = rs.string(forColumn: DBMedia.thumbUrl)
        size = Int(rs.int(forColumn: DBMedia.size))
        width = Int(rs.int(forColumn: DBMedia.width))
        height = Int(rs.int(forColumn: DBMedia.height))
        if let urlString = rs.string(forColumn: DBMedia.assetUrl) {
            assetUrl = URL(string: urlString)
        }
    }

    override func bindInsert() {
        args[DBMedia.messageUuid] = messageUuid
        args[DBMedia.order] = order
        args[DBMedia.type] = type.rawValue
    }

    override func bindUpdate() {
        args[DBMedia.url] = url
        args[DBMedia.thumbUrl] = thumbUrl
        args[DBMedia.size] = size
        args[DBMedia.width] = width
        args[DBMedia.height] = height
        args[DBMedia.assetUrl] = assetUrl?.absoluteString
    }

}
