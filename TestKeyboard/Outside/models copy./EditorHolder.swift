import Foundation

class EditorHolder {
    var text: String?
    var medias: [Media]?
    var reply: Reply?
    var wildcard: Wildcard?

    init(text: String? = nil, medias: [Media]? = nil, reply: Reply? = nil, wildCard: Wildcard? = nil) {
        self.text = text
        self.medias = medias
        self.reply = reply
        self.wildcard = wildCard
    }

    func isEmpty() -> Bool {
        return text == nil && medias == nil && reply == nil && wildcard == nil
    }
}
