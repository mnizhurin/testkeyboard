import Foundation
import ObjectMapper

struct TypingData: Mappable {

    var membershipId: Int!
    var name: String!
    var typing: Bool = false

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        membershipId <- map["membership_id"]
        name    <- map["name"]
        typing  <- map["typing"]
    }

}
