import Foundation
import ObjectMapper

class Reply: Model {
    var id: Int!
    var serial: Int!
    var userOrMembershipId: Int!
    var userName: String!
    var text: String!
    var replyForCurrentUser: Bool!
    private var mediaUrl: String?
    private var wildcardUrl: String?

    var smartMediaUrl: String? {
        get {
            return mediaUrl != nil ? mediaUrl : wildcardUrl
        }
    }

    init(message: ContentMessage) {
        id = message.id ?? 0
        serial = message.serial
        text = message.previewText

        if let user = message.user {
            userOrMembershipId = user.id
            userName = user.fullName
            replyForCurrentUser = user == UserManager.instance.currentUser
        } else if let membership = message.membership {
            userOrMembershipId = membership.id
            userName = membership.username
            replyForCurrentUser = membership.id == AppSession.instance.getMembershipIdByChannelId(channelId: message.channelId)
        }

        if let wildcard = message.wildcard as? ImageWildcard {
            wildcardUrl = wildcard.url
        }

        if let medias = message.medias, let media = medias.first {
            mediaUrl = media.thumbUrl
        }

        super.init()
    }

    required init?(map: Map) {
        super.init(map: map)
    }

    override func mapping(map: Map) {
        super.mapping(map: map)

        id                      <- map["id"]
        serial                  <- map["serial"]
        userOrMembershipId      <- map["user_or_membership_id"]
        userName                <- map["user_name"]
        text                    <- map["text"]
        mediaUrl                <- map["media_url"]
        replyForCurrentUser     <- map["reply_for_current_user"]
        wildcardUrl             <- map["wildcard_url"]
    }

}
