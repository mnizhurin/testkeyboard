import Foundation
import ObjectMapper

struct SocketErrorData: Mappable {

    var uuid: String!
    var error: String!

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        uuid    <- map["uuid"]
        error   <- map["error"]
    }

}
