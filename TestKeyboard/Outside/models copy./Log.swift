import Foundation

struct Log {

    let date: Date
    let content: String

    var formattedLog: String {
        let formattedDate = Singleton.instance.logDateFormatter.string(from: date)
        return "\(formattedDate): \(content)"
    }

    init(content: String) {
        self.date = Date()
        self.content = content
    }

}
