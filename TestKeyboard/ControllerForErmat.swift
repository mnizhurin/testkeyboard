import UIKit
import NextGrowingTextView

class ControllerForErmat: BaseKeyboardTrackerController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var editorView: NextGrowingTextView!
    @IBOutlet weak var editorHeight: NSLayoutConstraint!

    let searchBar = UISearchBar()

    @IBAction func toggle() {
//        tableView.isHidden = !tableView.isHidden
//        infoView.isHidden = !infoView.isHidden
        searchBar.endEditing(true)
        view.endEditing(true)
    }

    @IBAction func someAction() {
        self.pseudoAccessoryView.height = self.pseudoAccessoryView.height == 200 ? 44 : 200
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.isHidden = false
        infoView.isHidden = true

        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Enter text here"
        navigationItem.titleView = searchBar

        editorView.maxNumberOfLines = 7
        editorView.delegates.willChangeHeight = { [weak self] height in
            self?.editorHeight.constant = height
            self?.updateUI()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = "Line \(indexPath.row + 1)"
        return cell
    }

}
