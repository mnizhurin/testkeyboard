import Foundation
import RxSwift

extension Observable {

    func subscribe(_ disposeBag: DisposeBag, onNext: ((E) -> Void)? = nil, onError: ((Error) -> Void)? = nil, onCompleted: (() -> Void)? = nil, onDisposed: (() -> Void)? = nil) {
        self.subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed)
            .addDisposableTo(disposeBag)
    }

    func subscribeInBackground(_ disposeBag: DisposeBag, onNext: ((E) -> Void)? = nil, onError: ((Error) -> Void)? = nil, onCompleted: (() -> Void)? = nil, onDisposed: (() -> Void)? = nil) {
        self.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed)
            .addDisposableTo(disposeBag)
    }

    func subscribeDisposableInBackground(onNext: ((E) -> Void)? = nil, onError: ((Error) -> Void)? = nil, onCompleted: (() -> Void)? = nil, onDisposed: (() -> Void)? = nil) -> Disposable {
        return self.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed)
    }

    func subscribeInController(_ disposeBag: DisposeBag, onNext: ((E) -> Void)? = nil, onError: ((Error) -> Void)? = nil, onCompleted: (() -> Void)? = nil, onDisposed: (() -> Void)? = nil) {
        self.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed)
            .addDisposableTo(disposeBag)
    }

    func subscribeDisposableInController(_ disposeBag: DisposeBag, onNext: ((E) -> Void)? = nil, onError: ((Error) -> Void)? = nil, onCompleted: (() -> Void)? = nil, onDisposed: (() -> Void)? = nil) -> Disposable {
        let disposable = self.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed)
        disposeBag.insert(disposable)
        return disposable
    }

}
